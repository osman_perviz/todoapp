<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo_list extends Model {


    protected $fillable = [
        'name',
        'user_id',
        'publish'
    ];


 public function user()
 {
    return $this->belongsTo('App\User');
 }

 public function tasks()
 {
     return $this->hasMany('App\Task');
 }

}
