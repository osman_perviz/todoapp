<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {





    protected $fillable = [
        'body',
        'todo_list_id',
        'done'

    ];


    public function todo_list()
    {
        return $this->belongsTo('App\Todo_list');
    }

}
