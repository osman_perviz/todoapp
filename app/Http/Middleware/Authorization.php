<?php namespace App\Http\Middleware;

use App\Http\Requests\Request;
use App\Todo_list;
use App\Task;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class Authorization
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->route()->parameter('tasks')) {

            $task_id = $request->route()->parameter('tasks');
            $task = Task::findOrFail($task_id);
            $owner = $task->todo_list->user;

            if($request->ajax() && $owner != Auth::user())
            {
                return response('Unauthorized Request', 401);
            }

            if ($owner != Auth::user()) {
                session()->flash('flash_message', 'Unauthorize Request');
                return redirect('/');
            }

            return $next($request);
        }


        if($request->route()->parameter('todo_lists'))
        {
            $list_id =$request->route()->Parameter('todo_lists');
            $list = Todo_list::findOrFail($list_id);
            $user= $list->user;

            if($request->ajax() && $list->user != Auth::user())
            {
                return response('Unauthorized Request', 401);
            }

            if($user != Auth::user())
            {
                session()->flash('flash_message','Unauthorized Requests ');
                return redirect('/');
            }

            return $next($request);
        }

        return $next($request);
    }

}
