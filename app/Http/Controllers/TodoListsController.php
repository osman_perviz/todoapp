<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Todo_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;








class TodoListsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function __construct()
    {
        $this->middleware('authorization',['only'=>['update','edit','show','destroy']]);

    }

    public function index()
	{
        $lists = Todo_list::where('user_id','=', Auth::user()->id)->orWhere('publish','=','true')->get();
		return view('todo_lists.index',compact('lists'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('todo_lists.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request,['name'=>'required']);
        $todo_list= new Todo_list($request->all());

        Auth::user()->todo_lists()->save($todo_list);

        session()->flash('flash_message','New ToDoList Created');
        return redirect('todo_lists');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $list = Todo_list::findOrFail($id);
        $list_tasts = $list->tasks;
        return view('todo_lists.show',compact('list_tasts','list'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $list = Todo_list::findorFail($id);
        return view('todo_lists.edit',compact('list'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
        if($request->ajax()){
            $list = Todo_list::findOrFail($id);
            $this->validate($request,['name'=>'required']);
            $list->update($request->all());
            return response('Updated', 401);
        }
        $list = Todo_list::findOrFail($id);
		$this->validate($request,['name'=>'required']);
        $list->update($request->all());

        session()->flash('flash_message',' ToDoList Updated');
        return redirect('todo_lists');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $list= Todo_list::findOrFail($id);

        $list->delete();
        session()->flash('flash_message',' List are Deleted');
        return redirect('todo_lists');

	}

}
