<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Task;
use App\Todo_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function __construct()
    {
        $this->middleware('authorization',['only'=>['edit','show','destroy','update']]);
    }


	public function index()
	{
		return view('tasks.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(!count(Auth::user()->todo_lists) > 0)
        {
            session()->flash('flash_message','First Create a ToDoList');
            return redirect('/todo_lists/create');
        }

        $lists = Todo_list::where('user_id','=', Auth::user()->id)->lists('name','id');
        return view('tasks.create',compact('lists'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request,['body'=>'required','todo_list_id'=>'required']);

        Task::create($request->all());
        return redirect('/');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $task = Task::findOrFail($id);
        $lists = Todo_list::where('user_id','=', Auth::user()->id)->lists('name','id');
        return view('tasks.edit',compact('task','lists'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{

        if($request->ajax())
        {
            $task = Task::findOrFail($id);
            $this->validate($request,['done'=>'required']);
            $task->update($request->all());
            return response('Task are updated', 200);
        }
        $task = Task::findOrFail($id);
        $this->validate($request,['body'=>'required','todo_list_id'=>'required']);

        $task->update($request->all());

        return redirect('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $task = Task::findOrFail($id);
        $task->delete();
        return response('Task Deleted',200);
	}

}
