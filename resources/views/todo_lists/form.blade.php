
        <div class="form-group">
             {!! Form::label('name','Name:') !!}
             {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
          <div class="form-group">
             {!! Form::label('publish','Publish:') !!}
             {!! Form::select('publish',['false','true'],null,['class'=>'form-control']) !!}
         </div>
         <div class="form-group">
              {!! Form::submit($submitText,['class'=>'btn btn-primary form-control'])!!}
         </div>