@extends('layouts.app')

@section('content')

    <div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 createTask">
      <div class="panel panel-danger ">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Your ToDoList's</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($lists as $list)
                            <tr>
                                <td>{{$list->name}}</td>
                                <td>
                                    {!! Form::open(['method'=>'DELETE','route'=>['todo_lists.destroy',$list->id]]) !!}
                                        {!! Form::submit('Delete',['class'=>'btn btn-danger pull-right actionButton']) !!}
                                    {!! Form::close()!!}
                                     <a href="todo_lists/{{$list->id}}/edit" class="btn btn-success pull-right actionButton">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr>
                <a href="/todo_lists/create" class="btn btn-warning">Add New List</a>
            </div>
      </div>
    </div>

@stop