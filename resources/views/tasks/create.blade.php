@extends('layouts.app')

@section('content')


<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 createTask">
      <div class="panel panel-danger ">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Add Task</h3>
            </div>
            <div class="panel-body">
              {!! Form::open(['url'=>'tasks']) !!}
                  @include('tasks.form',['submitText'=>'Add Task'])
              {!! Form::close() !!}
            </div>
      </div>
</div>
@stop

