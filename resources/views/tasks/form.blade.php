        <div class="form-group">
            {!! Form::label('body','Task') !!}
            {!! Form::text('body',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('todo_list_id','Task List') !!}
            {!! Form::select('todo_list_id',$lists,null,['class'=>'form-control ']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit($submitText,['class'=>'btn btn-primary form-control']) !!}
        </div>



