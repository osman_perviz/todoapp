@extends('layouts.app')

@section('content')
<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 createTask">
      <div class="panel panel-danger ">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Edit Task</h3>
            </div>
            <div class="panel-body">
              {!! Form::model($task,['method'=>'PUT','action'=>['TasksController@update',$task->id]]) !!}
                  @include('tasks.form',['submitText'=>'Edit Task'])
              {!! Form::close() !!}
            </div>
      </div>
</div>
@stop