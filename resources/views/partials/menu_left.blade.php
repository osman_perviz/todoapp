 <div class="panel panel-success menuLeft">
    <div class="panel-heading">
        <h3 class="panel-title">ToDo Lists</h3>
    </div>
    <div class="panel-body">
    @if(count($lists))
        <ul class="list-group">
            @foreach($lists as $list)
                <li><a href="/todo_lists/{{$list->id}}" id="{{$list->id}}"class="list-group-item ajaxList"> {{$list->name}}</a></li>
            @endforeach
        </ul>
     @else
        <li class="list-group-item">No Lists Present</li>
        <a href="/todo_lists/create" class="btn btn-warning form-control"> Create Your First List</a>
     @endif
    </div>
</div>