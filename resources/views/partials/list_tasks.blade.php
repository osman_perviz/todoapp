
 @foreach($tasks as $task)
        <li>
            {!! Form::open(['method'=>'PUT']) !!}
                    {!! Form::checkbox('done',$task->done ,$task->done == 'true' ? 'checked' : '',['id'=>$task->id,'class'=>'pull-left ']) !!}
                    <p id="task_{{$task->id}}" class=" list-group-item task  text-capitalize {{ $task->done == 'true' ? 'done' : 'undone' }}">{{$task->body}}
                        <a href="/tasks/{{$task->id}}" class="deleteTask" id="task_{{$task->id}}"><span class='glyphicon glyphicon-trash pull-right'></span> </a>
                        <a href="/tasks/{{$task->id}}/edit" class=" editTask pull-right "><span class="glyphicon glyphicon-pencil"></span></a>
                    </p>
            {!! Form::close() !!}
        </li>
   @endforeach
