
    <div class="panel panel-danger menuRight">
        <div class="panel-heading">
          {!! Form::model($list,['method'=>'PUT','action'=>['TodoListsController@update',$list->id],'id'=>'publishForm']) !!}
            {!! Form::hidden('publish',$list->publish) !!}
            {!! Form::hidden('name',$list->name) !!}
            {!! Form::submit(($list->publish == 'true' || $list->publish == 1 )? 'Published' : 'Unpublished',['class'=>'btn btn-default-sm,pull-left']) !!}
            {{--<span class="glyphicon glyphicon-saved"></span>--}}
          </a>
            {!! Form::close() !!}
            <h3 class="panel-title text-center text-capitalize">{{$list->name}} List Tasks</h3>
        </div>
        @if(count($tasks))
            <ul class="list-group task_lists">
                @include('partials.list_tasks',['$tasks'=>$tasks])
            </ul>
        @else
            <h3 class="text-center">No Tasks Present</h3>
        @endif
    </div>

