$(document).ready(function(){

        $('.ajaxList').on('click',function(){
            event.preventDefault();
            var id = $(this).attr('id');
            $('.active').removeClass('active');
            $(this).addClass('active');

            $.ajax({
                url:'/todo_lists/' + id,
                type:'get',
                success:function(data) {
                    $('.rightSide').empty().html(data);
                },
                error: function(data){
                    $('.rightSide').empty().html("<div class='alert alert-danger'>" +  data.responseText + "</div>")
                }

            });

        });

       $(document).on("click","input[name=done]",function(){
            var checkbox = $(this);
            var checkBoxValue= (checkbox.val() === 'false' ) ? checkbox.attr('value',true) : checkbox.attr('value',false);
            var id = $(this).attr('id');
            var current_task= $('#task_' + id);
           $.ajax({
               url: '/tasks/' + id,
               type: 'PUT',
               cache: false,
               data: {
                   'done' : checkBoxValue.val()
               },
               beforeSend: function(request) {
                   return request.setRequestHeader('X-CSRF-Token', $("input[name='_token']").attr('value'));
               },
               success: function(data){
                   ($(current_task).hasClass('done')) ? $(current_task).removeClass('done').addClass('undone') : $(current_task).removeClass('undone').addClass('done')
               },
               error: function(data){
                   alert('errror')
               }
           })
       });


       $(document).on('click','.deleteTask',function(){
           event.preventDefault();
           var current = $(this);
           alert('Are you sure you want to delete this task')
           var path = current.attr('href')
           $.ajax({
               url:path,
               type:'DELETE',
               cache: false,
               beforeSend: function(request) {
                   return request.setRequestHeader('X-CSRF-Token', $("input[name='_token']").attr('value'));
               },
               success: function()
               {
                   current.closest('li').fadeOut('slow').remove();
               }

           })
       });

            //$(document).on('click','.publish',function()
            $(document).on('submit','#publishForm',function()
            {
                event.preventDefault();
                var path = $(this).attr('action');
                var publish = $('input[name=publish]');
                var submit =  $('input[type=submit]');
                var name = $('input[name=name]').val();
                if (publish.val() != 1)
                {
                    publish.attr('value',1)
                    submit.attr('value','Published')
                }else{
                        publish.attr('value',0)
                        submit.attr('value','Unpublished')
                };

                $.ajax({
                    url: path,
                    type: 'PUT',
                    cache: false,
                    data: {
                        'publish':publish.val(),
                        'name':name
                    },
                    beforeSend: function(request) {
                        return request.setRequestHeader('X-CSRF-Token', $("input[name='_token']").attr('value'));
                    },
                    success: function()
                    {
                        alert('Neradi')
                    }
                })
            });




});//document.ready
