<?php
use Illuminate\Database\Seeder;
class TodoListsTableSeeder extends Seeder{

    public function run()
    {
        DB::table('todo_lists')->delete();

        $list = [

            [
                'name'=>'user_1 first',
                'user_id'=> 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name'=>'user_1 second',
                'user_id'=> 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name'=>'user_2 first',
                'user_id'=> 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name'=>'user_2 second',
                'user_id'=> 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name'=>'user_3 first',
                'user_id'=> 3,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'name'=>'user_3 second',
                'user_id'=> 3,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]


        ];

        DB::table('todo_lists')->insert($list);
    }
}