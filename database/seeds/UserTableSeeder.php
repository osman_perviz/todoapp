<?php
use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();

        $users = array(
            array(
                'name'=>'user_1',
                'email'=>'user_1@gmail.com',
                'password'=>Hash::make('user_1'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ),
            array(
                'name'=>'user_2',
                'email'=>'user_2@gmail.com',
                'password'=>Hash::make('user_2'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ),
            array(
                'name'=>'user_3',
                'email'=>'user_3@gmail.com',
                'password'=>Hash::make('user_3'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            )
        );

        DB::table('users')->insert($users);

    }
}