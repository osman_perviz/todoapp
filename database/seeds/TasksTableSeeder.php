<?php
use Illuminate\Database\Seeder;
class TasksTableSeeder extends Seeder{

    public function run()
    {
        DB::table('tasks')->delete();

        $tasks = [

            [
                'body'=>'task-1',
                'todo_list_id'=>1,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>1,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>1,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-1',
                'todo_list_id'=>2,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>2,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>2,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-1',
                'todo_list_id'=>3,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>3,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>3,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-1',
                'todo_list_id'=>4,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>4,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>4,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-1',
                'todo_list_id'=>5,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>5,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>5,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-1',
                'todo_list_id'=>6,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-2',
                'todo_list_id'=>6,
                'done'=>0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [
                'body'=>'task-3',
                'todo_list_id'=>6,
                'done'=>1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]

        ];
        DB::table('tasks')->insert($tasks);
    }


}